# R329-Tina-jishu

> 注：R329 SDK来源官方GitHub, 本仓库已经同步最新完整[官方SDK](https://github.com/sipeed/R329-Tina-jishu.git)

## SDK编译环境 Docker

docker pull tdleiyao/ubuntu-sipeed_r329_env:bionic

docker run --name=Ubuntu_R329_TD -v /宿主机仓库路径:/容器内仓库路径 -ti tdleiyao/ubuntu-sipeed_r329_env:bionic bash

## ZouYi AIPU Docker

使用矽速科技提供的docker环境进行开发：
> 注：请保证至少有20GB的空闲磁盘空间
```
# 方法一，从docker hub下载，需要梯子
sudo docker pull zepan/zhouyi
# 方法二，百度云下载镜像文件（压缩包约2.9GB，解压后约5.3GB）
# 链接：https://pan.baidu.com/s/1yaKBPDxR_oakdTnqgyn5fg 
# 提取码：f8dr 
gunzip zhouyi_docker.tar.gz
sudo docker load --input zhouyi_docker.tar
```
下载好docker后即可运行其中的例程测试环境是否正常：
```
sudo docker run -i -t zepan/zhouyi  /bin/bash

cd ~/demos/tflite
./run_sim.sh
python3 quant_predict.py
```

## 编译参考

全志 R329官方 

https://r329.docs.allwinnertech.com

其他参考

https://www.cnblogs.com/juwan/p/14650733.html

## SDK路径

官方SDK GitHub链接
```
https://github.com/sipeed/R329-Tina-jishu.git
```

#### 模块路径
> 注：已将github.com替换为hub.fastgit.org，便于单独下载 （GitHub 国内下载有时很慢)
```
https://e.coding.net/sipeed_ai/r329/build.git
https://e.coding.net/sipeed_ai/r329/config.git
https://e.coding.net/sipeed_ai/r329/dl.git
https://e.coding.net/sipeed_ai/r329/brandy-2.0.git
https://hub.fastgit.org/sipeed/r329-linux-4.9.git
https://hub.fastgit.org/sipeed/r329-package.git
https://e.coding.net/sipeed_ai/r329/prebuilt.git
https://e.coding.net/sipeed_ai/r329/scripts.git
https://hub.fastgit.org/sipeed/r329-target.git
https://e.coding.net/sipeed_ai/r329/toolchain.git
https://e.coding.net/sipeed_ai/r329/tools.git
```

2021-7-22

By AlgoIdeas
